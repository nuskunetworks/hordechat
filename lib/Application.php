<?php
/**
 * Copyright (C) 2011 Nusku Networks
 * Copyright (C) Mike Crosson <mcrosson_cloud <at> nusku <dot> net>
 *
 * See the enclosed file COPYING for license information (GPL). If you
 * did not receive this file, see http://www.fsf.org/copyleft/gpl.html.
 */


/* Determine the base directories. */
if (!defined('CHAT_BASE')) {
    define('CHAT_BASE', dirname(__FILE__) . '/..');
}

if (!defined('HORDE_BASE')) {
    /* If Horde does not live directly under the app directory, the HORDE_BASE
     * constant should be defined in config/horde.local.php. */
    if (file_exists(CHAT_BASE . '/config/horde.local.php')) {
        include CHAT_BASE . '/config/horde.local.php';
    } else {
        define('HORDE_BASE', CHAT_BASE . '/..');
    }
}

/* Load the Horde Framework core (needed to autoload
 * Horde_Registry_Application::). */
require_once HORDE_BASE . '/lib/core.php';

class Chat_Application extends Horde_Registry_Application
{
    /**
     */
    public $version = '1.0 git';

    /**
     * Global variables defined:
     * - $variable: List all global variables here.
     */
    protected function _init()
    {
    }

    /**
     */
    public function menu($menu)
    {
        $menu->add(Horde::url('chat.php'), _("Chat"), 'user.png');
    }

    public function configSpecialValues($what)
    {
        switch ($what) {
        case 'groups':
            $groups = Horde_Array::valuesToKeys($GLOBALS['injector']
                                                ->getInstance('Horde_Group')
                                                ->listAll());
            asort($groups);
            return $groups;

        case 'users':
            try {
                $users = Horde_Array::valuesToKeys($GLOBALS['injector']
                                                    ->getInstance('Horde_Core_Factory_Auth')
                                                    ->create()
                                                    ->listUsers());
            }
            catch (Horde_Auth_Exception $e) {
                return null;
            }
            asort($users);
            return $users;
        }
    }

}
