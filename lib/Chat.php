<?php
/**
 * Copyright (C) 2011 Nusku Networks
 * Copyright (C) Mike Crosson <mcrosson_cloud <at> nusku <dot> net>
 *
 * See the enclosed file COPYING for license information (GPL). If you
 * did not receive this file, see http://www.fsf.org/copyleft/gpl.html.
 */

class Chat
{
    /**
     * Returns the user who is accessing chat
     *
     * @param boolean $full  Always return the full user name with realm?
     *
     * @return string  The current user.
     */
    static public function getUser($full = true)
    {
        $user = $GLOBALS['registry']->getAuth($full ? 'bare' : null);
        
        return $user;
    }
}