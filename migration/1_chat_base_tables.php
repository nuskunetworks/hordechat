<?php
/**
 * Copyright (C) 2011 Nusku Networks
 * Copyright (C) Mike Crosson <mcrosson_cloud <at> nusku <dot> net>
 *
 * See the enclosed file COPYING for license information (GPL). If you
 * did not receive this file, see http://www.fsf.org/copyleft/gpl.html.
 */

/*
class ChatBaseTables extends Horde_Db_Migration_Base
{

    public function up()
    {
        $tableList = $this->tables();

        // TODO: Make sure the below setup actually generates what we expect on the database engine side 

        if (!in_array('chat_pfc_storage', $tableList)) {
            $t = $this->createTable('chat_pfc_storage', array('autoincrementKey' => false));
            $t->column('server', 'string', array('limit' => 32, 'null' => false, 'default' => ''));
            $t->column('group', 'string', array('limit' => 64, 'null' => false, 'default' => ''));
            $t->column('subgroup', 'string', array('limit' => 128, 'null' => false, 'default' => ''));
            $t->column('leaf', 'string', array('limit' => 128, 'null' => false, 'default' => ''));
            $t->column('leafvalue', 'text', array('null' => false));
            $t->column('timestamp', 'int', array('limit' => 11, 'null' => false, 'default' => 0));

            $t->primaryKey(array('server', 'group', 'subgroup', 'leaf'));
            $t->end();

            $this->addIndex('chat_pfc_storage', array('server', 'group', 'subgroup', 'timestamp' ));
        }
    }

   public function down()
    {
        $this->dropTable('chat_pfc_storage');
    }

}
*/
