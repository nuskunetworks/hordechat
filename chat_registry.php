<?php
/**
Copyright (C) 2011 Nusku Networks
Copyright (C) 2011 Mike Crosson <mcrosson_cloud <at> nusku <dot> net>


This file is part of  cloud.nusku.net chat.

cloud.nusku.net chat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License.

cloud.nusku.net chat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cloud.nusku.net chat.  If not, see <http://www.gnu.org/licenses/>.

*/

// Register the application in the sidebar of Horde
$this->applications['chat'] = array(
        'name' => _("Chat"),
        'provides' => 'Chat',
    );

