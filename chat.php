<?php
/**
 * Copyright (C) 2011 Nusku Networks
 * Copyright (C) Mike Crosson <mcrosson_cloud <at> nusku <dot> net>
 *
 * See the enclosed file COPYING for license information (GPL). If you
 * did not receive this file, see http://www.fsf.org/copyleft/gpl.html.
 */

require_once dirname(__FILE__) . '/lib/Application.php';
Horde_Registry::appInit('chat');

$page_output->header(array(
    'body_class' => 'form',
));

$page_output->topbar = $page_output->sidebar = false;

if (! isset($_REQUEST['pfc_ajax'])) {
	$title = $params["title"] = $conf['general']['title'];
	if ($conf['general']['debug']) {
		Horde::logMessage('---' . $title . '---', 'DEBUG');
	}
	$notification->notify(array('listeners' => 'status'));
}

require_once dirname(__FILE__) . "/pfc.php";

$page_output->footer();

?>
