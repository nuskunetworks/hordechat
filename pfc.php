<?php
/**
 * Copyright (C) 2011 Nusku Networks
 * Copyright (C) Mike Crosson <mcrosson_cloud <at> nusku <dot> net>
 *
 * See the enclosed file COPYING for license information (GPL). If you
 * did not receive this file, see http://www.fsf.org/copyleft/gpl.html.
 */


// This is needed so we can get the user's Horde id
require_once dirname(__FILE__) . '/lib/Application.php';
Horde_Registry::appInit('chat');

$vars = Horde_Variables::getDefaultVariables();

require_once dirname(__FILE__) . "/phpfreechat/src/phpfreechat.class.php";
$params = array();
$params["serverid"] = md5(__FILE__); // calculate a unique id for this chat
// Disable caching of parameters that come in from the horde config / user preferences
$params['dyn_params'] = array(
	'debug',
	'frozen_nick',
	'max_nick_len',
	'nick',
	'time_format',
	'date_format',
	'notify_window',
	'showsmileys',
	'btn_sh_smileys',
	'title',
	'displaytabclosebutton',
	'displaytabimage',
	'display_ping',
	'startwithsound',
	'clock',
	'nickmarker',
	'shownotice',
	'short_url',
	'short_url_width',
	'max_displayed_lines',
	'max_msg',
	'max_text_len',
	'timeout',
	'refresh_delay',
	'max_privmsg',
	'max_channels',
	'isadmin',
);

// Cheat a little bit TODO: Move this to Chat.php static function
if (empty($conf['general']['debug'])) {
	$conf['general']['debug'] = false;
	$params['display_ping'] = false;
	$params['debug'] = false;
}

// Enable debugging
///////////////////
if ($conf['general']['debug']) {
	$params['debug'] = true;
	$params['display_ping'] = true;
}

// Prevent users from changing nickname
///////////////////////////////////////
$params["frozen_nick"] = !(bool)$conf['nick']['user_change']; // TODO: Move call to user_change to Chat.php static function

// Set the maximum length of a nickname
///////////////////////////////////////
$params['max_nick_len'] = empty($conf['nick']['max_nick_len']) ? 15: intval($conf['nick']['nick_max_length']);

// Set initial nickname
///////////////////////
if (!(bool)$conf['nick']['user_change']) {
	$params['nick'] = Chat::getUser(false);
}
else {
	$params["nick"] = empty($prefs['nick']) ? Chat::getUser(false) : $prefs['nick'];
}

// Set format of time
/////////////////////
if (!empty($prefs['time_format'])) {
	$params['time_format'] = $prefs['time_format'];
}

// Set format of date
/////////////////////
if (!empty($prefs['date_format'])) {
	$params['date_format'] = $prefs['date_format'];
}

// Set whether or not notifications appear in title of window
/////////////////////////////////////////////////////////////
$params['notify_window'] = empty($prefs['title_notif']) ? true : (bool)$prefs['title_notif'];

// Smiley's user / config settings
//////////////////////////////////
$params['showsmileys'] = empty($prefs['smiley_selector']) ? false : (bool)$prefs['smiley_selector'];
$params['btn_sh_smileys'] = empty($conf['general']['smileys']) ? false : (bool)$conf['general']['smileys'];
// Disable smiley's bar if button disabled
if (!$params['btn_sh_smileys']) {
	$params['showsmileys'] = false;
}

// Chat Title
/////////////
$params["title"] = $conf['general']['title'];

// Display tab close button
///////////////////////////
$params['displaytabclosebutton'] = empty($prefs['tab_close_button']) ? false : (bool)$prefs['tab_close_button'];

// Display tab image
////////////////////
$params['displaytabimage'] = empty($prefs['tab_image']) ? false : (bool)$prefs['tab_image'];

// Sound
////////
$params['startwithsound'] = empty($prefs['enable_sound']) ? false : (bool)$prefs['enable_sound'];

// Clock (date/hour column)
///////////////////////////
$params['clock'] = empty($prefs['date_hour_column']) ? true : (bool)$prefs['date_hour_column'];

// Nickname colorization
////////////////////////
$params['nickmarker'] = empty($prefs['nick_color']) ? false : (bool)$prefs['nick_color'];
if ($conf['general']['debug']) {
	Horde::logMessage('---' . $prefs['nick_color'] . '---', 'DEBUG');
}
if ($conf['general']['debug']) {
	Horde::logMessage('---' . $params['nickmarker'] . '---', 'DEBUG');
}

// Notifications
////////////////
if (isset($prefs['notice'])) {
	$params['shownotice'] = intval($prefs['notice']);
}

// Short URLs
/////////////
$params['short_url'] = $conf['url']['shorten'];
$params['short_url_width'] = empty($conf['url']['width']) ? 40 : intval($conf['url']['width']);

// Max history in browser window
////////////////////////////////
$params['max_displayed_lines'] = isset($prefs['history_length']) ? intval($prefs['history_length']) : 150;

// Initial history to show
//////////////////////////
$params['max_msg'] = isset($conf['general']['initial_history']) ? intval($conf['general']['initial_history']) : 20;

// Message max length
/////////////////////
$params['max_text_len'] = isset($conf['messaging']['max_msg_length']) ? intval($conf['messaging']['max_msg_length']) : 400;

// Timeout
//////////
$params['timeout'] = isset($conf['timing']['timeout']) ? intval($conf['timing']['timeout']) : 35000;

// Refresh delay
////////////////
$params['refresh_delay'] = isset($conf['timing']['refresh']) ? intval($conf['timing']['refresh']) : 2000;

// Max concurrent private messages
//////////////////////////////////
$params['max_privmsg'] = isset($conf['messaging']['max_priv_msg']) ? intval($conf['messaging']['max_priv_msg']) : 5;

// Max channels
///////////////
$params['max_channels'] = isset($conf['general']['max_channels']) ? intval($conf['general']['max_channels']) : 10;

// Set administrative flag for user
// TODO: The below checks based on the intersection of group might be inefficient.  Appears to perform a large number of SQL calls when looking at DEBUG output
///////////////////////////////////
// Check user account's selected in configuration
$user = $GLOBALS['registry']->getAuth();
if (in_array($user, $conf['administrators']['users'])) {
	if ($conf['general']['debug']) {
		Horde::logMessage('Admin achieved by username', 'DEBUG');
	}
	$params['isadmin'] = true;
}

// Check groups selected in configuration
$userGroups = $GLOBALS['injector']->getInstance('Horde_Group')->listGroups($user);
$adminGroups = $conf['administrators']['groups'];
if (count(array_intersect($userGroups, $adminGroups)) > 0) {
	if ($conf['general']['debug']) {
		Horde::logMessage('Admin achieved via groups', 'DEBUG');
	}
	$params['isadmin'] = true;
}

// Database storage instead of file storage
///////////////////////////////////////////
//$params['container_type'] = 'horde';
//$params['container_cfg_mysql_host'] = 'localhost';
//$params['container_cfg_mysql_port'] = 3306;
//$params['container_cfg_mysql_database'] = 'horde';
//$params['container_cfg_mysql_table'] = 'phpfreechat';
//$params['container_cfg_mysql_username'] = 'horde';
//$params['container_cfg_mysql_password'] = 'horde';

// Misc options that are static
///////////////////////////////
$params["channels"] = array("General");
$params['skip_proxies'] = array('censor');
$params['server_script_path'] = 'pfc.php';
$params['admins'] = array();
$params['cmd_path'] = dirname(__FILE__) . '/lib/Commands/';
// Launch chat
//////////////
$chat = new phpFreeChat( $params );
$chat->printChat();

?>
